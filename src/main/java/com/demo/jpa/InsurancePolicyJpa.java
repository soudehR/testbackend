package com.demo.jpa;

import com.demo.model.PolicyStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Data model of an insurance policy
 */
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsurancePolicyJpa {
    /**
     * Insurance policy technical ID
     */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    /** Insurance name */
    @NonNull
    private String name;

    /** Insurance status */
    @Enumerated(EnumType.STRING)
    private PolicyStatus status;

    /** Start date of insurance cover */
    private LocalDateTime startDate;

    /** End date of insurance cover */
    private LocalDateTime endDate;

    /** Insurance policy creation date */
    private LocalDate creationDate;

    /** Insurance policy modification date */
    private LocalDateTime modificationDate;
}
