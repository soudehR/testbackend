package com.demo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InsurancePolicyRepository extends JpaRepository<InsurancePolicyJpa, Integer> {
}
