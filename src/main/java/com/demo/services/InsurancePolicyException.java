package com.demo.services;

public class InsurancePolicyException extends Exception {
    public InsurancePolicyException (String message) {
        super(message);
    }
}
