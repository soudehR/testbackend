package com.demo.services;

import com.demo.jpa.InsurancePolicyJpa;
import com.demo.jpa.InsurancePolicyRepository;
import com.demo.model.PolicyStatus;
import com.demo.ws.dto.InsurancePolicyDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InsurancePolicyServices {

    @Autowired
    private InsurancePolicyRepository repository;

    /**
     * Create a new insurance policy using dto sent in the parameter
     * @param dto DTO representing insurance policy to create
     * @return The created insurance policy
     */
    public InsurancePolicyDto createInsurancePolicy(InsurancePolicyDto dto) throws InsurancePolicyException { //DTO is used in this service to gain time. The best practice is using a BM for sure
        verifyMandatoryFields(dto);
        InsurancePolicyJpa createdInsurance = repository.save(InsurancePolicyJpa.builder()
                .name(StringUtils.isNoneBlank(dto.getName()) ? dto.getName() : String.join(" ", "Insurance n°", String.valueOf(System.currentTimeMillis())))
                .status(dto.getStatus() == null ? PolicyStatus.ACTIVE : dto.getStatus())
                .startDate(dto.getStartDate())
                .endDate(dto.getEndDate())
                .creationDate(LocalDate.now())
                .modificationDate(LocalDateTime.now())
                .build());

        return InsurancePolicyDto.builder()
                .idInsurance(createdInsurance.getId())
                .name(createdInsurance.getName())
                .status(createdInsurance.getStatus())
                .startDate(createdInsurance.getStartDate())
                .endDate(createdInsurance.getEndDate())
                .creationDate(createdInsurance.getCreationDate())
                .modificationDate(createdInsurance.getModificationDate())
                .build();
    }

    /**
     * Get all insurance policies
     * @return Insurance policies list
     */
    public List<InsurancePolicyDto> getInsurancePolicies() {
        List<InsurancePolicyJpa> jpas = repository.findAll();
        return jpas.stream().map(jpa -> InsurancePolicyDto.builder()
                .idInsurance(jpa.getId())
                .name(jpa.getName())
                .status(jpa.getStatus())
                .startDate(jpa.getStartDate())
                .endDate(jpa.getEndDate())
                .creationDate(jpa.getCreationDate())
                .modificationDate(jpa.getModificationDate())
                .build()).collect(Collectors.toList());
    }

    /**
     * Get the insurance policy having the technical ID sent in the parameter
     * @param idPolicy ID of the policy to get
     * @return The specific insurance policy
     */
    public Optional<InsurancePolicyDto> getInsurancePolicyById(int idPolicy) {
        Optional<InsurancePolicyJpa> policyOp = repository.findById(idPolicy);
        if (policyOp.isPresent()) {
            InsurancePolicyJpa policy = policyOp.get();
            return Optional.of(InsurancePolicyDto.builder()
                    .idInsurance(policy.getId())
                    .name(policy.getName())
                    .status(policy.getStatus())
                    .startDate(policy.getStartDate())
                    .endDate(policy.getEndDate())
                    .creationDate(policy.getCreationDate())
                    .modificationDate(policy.getModificationDate())
                    .build());
        }
        return Optional.empty();
    }

    /**
     * Update the insurance policy using DTO sent in the request body
     * @param policy DTO representing insurance policy to modify
     * @return The up-to-date insurance policy
     */
    public Optional<InsurancePolicyDto> updateInsurancePolicy(InsurancePolicyDto policy) throws InsurancePolicyException {

        if(policy == null || policy.getIdInsurance() == null) {
            return Optional.empty();
        }
        Optional<InsurancePolicyJpa> policyOp = repository.findById(policy.getIdInsurance());
        if(policyOp.isEmpty()) {
            return Optional.empty();
        }
        InsurancePolicyJpa policyToUpdate = policyOp.get();

        if(StringUtils.isNotBlank(policy.getName())) {
            policyToUpdate.setName(policy.getName());
        }

        if(policy.getStatus() != null) {
            policyToUpdate.setStatus(policy.getStatus());

        }
        verifyMandatoryFields(policy);

        policyToUpdate.setStartDate(policy.getStartDate());
        policyToUpdate.setEndDate(policy.getEndDate());
        policyToUpdate.setModificationDate(LocalDateTime.now());

       InsurancePolicyJpa policyUpdated = repository.save(policyToUpdate);

        return Optional.of(InsurancePolicyDto.builder()
                .idInsurance(policyUpdated.getId())
                .name(policyUpdated.getName())
                .status(policyUpdated.getStatus())
                .startDate(policyUpdated.getStartDate())
                .endDate(policyUpdated.getEndDate())
                .creationDate(policyUpdated.getCreationDate())
                .modificationDate(policyUpdated.getModificationDate())
                .build());
    }

    /**
     * Control the presence of mandatory fields
     */
    private void verifyMandatoryFields(InsurancePolicyDto dto) throws InsurancePolicyException {
        if(dto.getStartDate() == null) {
            throw new InsurancePolicyException("Insurance policy start date is mandatory");
        }
        if(dto.getEndDate() == null) {
            throw new InsurancePolicyException("Insurance policy end date is mandatory");
        }
        if(!dto.getStartDate().isBefore(dto.getEndDate())) {
            throw new InsurancePolicyException("Insurance policy start date cannot be after or equal the end date");
        }

    }
}
