package com.demo.ws.controller;

import com.demo.services.InsurancePolicyException;
import com.demo.services.InsurancePolicyServices;
import com.demo.ws.dto.InsurancePolicyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller of Insurance policies
 */
@RestController
@RequestMapping(path="/insurance-policies")
public class InsuranceController {

    @Autowired
    private InsurancePolicyServices services;

    /**
     * Create a new insurance policy using dto sent in the request body
     * @param insurancePolicyDto DTO representing insurance policy to create
     * @return The created insurance policy
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InsurancePolicyDto> createInsurancePolicy (@RequestBody InsurancePolicyDto insurancePolicyDto) throws InsurancePolicyException {
        InsurancePolicyDto policy = services.createInsurancePolicy(insurancePolicyDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(policy);
    }

    /**
     * Get all insurance policies
     * @return Insurance policies list
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<InsurancePolicyDto> getAllInsurancePolicies () {
        return services.getInsurancePolicies();
    }

    /**
     * Get the insurance policy having the technical ID sent in the request
     * @param idPolicy ID of the policy to get
     * @return The specific insurance policy
     */
    @GetMapping(path = "/{idPolicy}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InsurancePolicyDto> getInsurancePolicy (@PathVariable int idPolicy) {
        Optional<InsurancePolicyDto> policy = services.getInsurancePolicyById(idPolicy);
        return policy.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    /**
     * Update the insurance policy using DTO sent in the request body
     * @param insurancePolicyDto DTO representing insurance policy to modify
     * @return The up-to-date insurance policy
     */
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InsurancePolicyDto> updateInsurancePolicy (@RequestBody InsurancePolicyDto insurancePolicyDto) throws InsurancePolicyException {
        Optional<InsurancePolicyDto> insuranceUpdated = services.updateInsurancePolicy(insurancePolicyDto);
        return insuranceUpdated.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Handle the unexpected errors. In our case is required fields controls
     * @param e Exception
     * @return Response with specific status and message
     */
    @ExceptionHandler(InsurancePolicyException.class)
    public ResponseEntity<Object> handleError (InsurancePolicyException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
