package com.demo.ws.dto;

import com.demo.model.PolicyStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * DTO representing an insurance policy characteristics
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsurancePolicyDto {

    /** Technical ID of insurance policy */
    private Integer idInsurance;
    /** Insurance policy name */
    private String name;
    /** Insurance policy status */
    private PolicyStatus status;
    /** Start date of insurance cover */
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private LocalDateTime startDate;
    /** End date of insurance cover */
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private LocalDateTime endDate;
    /** Insurance policy creation date */
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate creationDate;
    /** Insurance policy modification date */
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private LocalDateTime modificationDate;
}
