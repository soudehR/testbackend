package com.demo.model;

/**
 * Enum representing the possibles status of an insurance policy
 */
public enum PolicyStatus {
    ACTIVE,
    INACTIVE
}
