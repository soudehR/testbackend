package com.demo.ws.controller;

import com.demo.model.PolicyStatus;
import com.demo.services.InsurancePolicyException;
import com.demo.services.InsurancePolicyServices;
import com.demo.ws.dto.InsurancePolicyDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = InsuranceController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
@RunWith(SpringRunner.class)
@AutoConfigureRestDocs("target/generated-snippets")
@TestPropertySource(properties = {"spring.cloud.config.enabled=false"})
public class InsuranceControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private InsurancePolicyServices insurancePolicyServices;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        /* When */
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @Test
    public void testCreateInsurancePolicy() throws Exception {

        /* Given */
        InsurancePolicyDto dto = InsurancePolicyDto.builder()
                .name("policyTest")
                .startDate(LocalDateTime.now().minusHours(1))
                .endDate(LocalDateTime.now().minusYears(1))
                .build();

        InsurancePolicyDto dtoCreated = InsurancePolicyDto.builder()
                .idInsurance(1)
                .name("policyTest")
                .status(PolicyStatus.ACTIVE)
                .startDate(LocalDateTime.now().minusHours(1))
                .endDate(LocalDateTime.now().plusYears(1))
                .creationDate(LocalDate.now())
                .modificationDate(LocalDateTime.now())
                .build();
        when(insurancePolicyServices.createInsurancePolicy(any(InsurancePolicyDto.class))).thenReturn(dtoCreated);

        /* When */
        mvc.perform(RestDocumentationRequestBuilders.post("/insurance-policies")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andDo(document("create-insurance-policy-200", getDescriptionResponseFields("")));

        /* Then */
        verify(insurancePolicyServices).createInsurancePolicy(any(InsurancePolicyDto.class));
    }

    @Test
    public void testCreateInsurancePolicy_400_requiredFieldMissing() throws Exception {
        /* Given */
        InsurancePolicyDto dto = InsurancePolicyDto.builder()
                .name("policyTest")
                .endDate(LocalDateTime.now().minusYears(1))
                .build();
        when(insurancePolicyServices.createInsurancePolicy(any())).thenThrow(new InsurancePolicyException("Insurance policy start date is mandatory"));

        /* When */
        mvc.perform(MockMvcRequestBuilders.post("/insurance-policies")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Insurance policy start date is mandatory"));
    }

    @Test
    public void testModifyInsurancePolicy() throws Exception {

        /* Given */
        InsurancePolicyDto dto = InsurancePolicyDto.builder()
                .idInsurance(1)
                .name("policyTest")
                .status(PolicyStatus.ACTIVE)
                .startDate(LocalDateTime.now().minusHours(1))
                .endDate(LocalDateTime.now().plusYears(1))
                .creationDate(LocalDate.now())
                .modificationDate(LocalDateTime.now())
                .build();
        when(insurancePolicyServices.updateInsurancePolicy(any(InsurancePolicyDto.class))).thenReturn(Optional.of(dto));

        /* When */
        mvc.perform(RestDocumentationRequestBuilders.put("/insurance-policies")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andDo(document("update-insurance-policy-200", getDescriptionResponseFields("")));

        /* Then */
        verify(insurancePolicyServices).updateInsurancePolicy(any(InsurancePolicyDto.class));
    }

    @Test
    public void testGetAllInsurancePolicies() throws Exception {

        /* Given */
        InsurancePolicyDto dto = InsurancePolicyDto.builder()
                .idInsurance(1)
                .name("policyTest")
                .status(PolicyStatus.ACTIVE)
                .startDate(LocalDateTime.now().minusHours(1))
                .endDate(LocalDateTime.now().minusYears(1))
                .creationDate(LocalDate.now())
                .modificationDate(LocalDateTime.now())
                .build();
        when(insurancePolicyServices.getInsurancePolicies()).thenReturn(Collections.singletonList(dto));

        /* When */

        mvc.perform(RestDocumentationRequestBuilders.get("/insurance-policies")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andDo(document("get-insurance-policies-200", getDescriptionResponseFields("[].")));

        /* Then */
        verify(insurancePolicyServices).getInsurancePolicies();
    }

    @Test
    public void testGetInsurancePolicy() throws Exception {

        /* Given */
        InsurancePolicyDto dto = InsurancePolicyDto.builder()
                .idInsurance(1)
                .name("policyTest")
                .status(PolicyStatus.ACTIVE)
                .startDate(LocalDateTime.now().minusHours(1))
                .endDate(LocalDateTime.now().minusYears(1))
                .creationDate(LocalDate.now())
                .modificationDate(LocalDateTime.now())
                .build();
        when(insurancePolicyServices.getInsurancePolicyById(1)).thenReturn(Optional.of(dto));

        /* When */

        mvc.perform(RestDocumentationRequestBuilders.get("/insurance-policies/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andDo(document("get-insurance-policy-200", getDescriptionResponseFields("")));

        /* Then */
        verify(insurancePolicyServices).getInsurancePolicyById(1);
    }

    @Test
    public void testGetInsurancePolicy_notExist() throws Exception {

        /* Given */
        when(insurancePolicyServices.getInsurancePolicyById(1)).thenReturn(Optional.empty());

        /* When */

        mvc.perform(MockMvcRequestBuilders.get("/insurance-policies/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());

        /* Then */
        verify(insurancePolicyServices).getInsurancePolicyById(1);
    }

    private ResponseFieldsSnippet getDescriptionResponseFields(String prefix) {
        List<FieldDescriptor> fieldDescriptors = Arrays.asList(
                fieldWithPath(prefix + "idInsurance").description("Insurance ID").type(JsonFieldType.NUMBER),
                fieldWithPath(prefix + "name").description("Descriptive name of insurance policy").type(JsonFieldType.STRING).optional(),
                fieldWithPath(prefix + "status").description("Status of insurance policy. Possibles values are : " +
                        Arrays.stream(PolicyStatus.values()).map(Enum::name).collect(Collectors.joining(", "))).type(JsonFieldType.STRING),
                fieldWithPath(prefix + "startDate").description("Start date of insurance cover").type(JsonFieldType.STRING),
                fieldWithPath(prefix + "endDate").description("End date of insurance cover").type(JsonFieldType.STRING),
                fieldWithPath(prefix + "creationDate").description("Insurance policy creation date").type(JsonFieldType.STRING),
                fieldWithPath(prefix + "modificationDate").description("Last insurance policy modification date").type(JsonFieldType.STRING));
        return responseFields(fieldDescriptors);
    }
}
