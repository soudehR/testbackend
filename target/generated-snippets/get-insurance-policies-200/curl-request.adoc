[source,bash]
----
$ curl 'http://localhost:8080/insurance-policies' -i -X GET \
    -H 'Accept: application/json'
----