|===
|Path|Type|Description

|`+idInsurance+`
|`+Number+`
|Insurance ID

|`+name+`
|`+String+`
|Descriptive name of insurance policy

|`+status+`
|`+String+`
|Status of insurance policy. Possibles values are : ACTIVE, INACTIVE

|`+startDate+`
|`+String+`
|Start date of insurance cover

|`+endDate+`
|`+String+`
|End date of insurance cover

|`+creationDate+`
|`+String+`
|Insurance policy creation date

|`+modificationDate+`
|`+String+`
|Last insurance policy modification date

|===